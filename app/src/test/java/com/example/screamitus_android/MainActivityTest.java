



// Group Members

// 1. Melvin John - C0742327
// 2. Akhil Somaraj - C0721763





package com.example.screamitus_android;


import android.widget.Button;
import android.widget.EditText;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertNotNull;



@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);
    }

    @Test
    public void testTextBoxAndButtonVisible() throws Exception {
        EditText days = (EditText) activity.findViewById(R.id.daysTextBox);

        assertNotNull(days);
    }

}
