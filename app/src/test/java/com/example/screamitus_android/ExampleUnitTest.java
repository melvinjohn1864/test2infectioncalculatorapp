package com.example.screamitus_android;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.Robolectric;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    private Infection infection;

    @Before
    public void setUp() throws Exception {
        infection = new Infection();

    }

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testNumberOfDaysGreaterThanZero() {
        //R1 - Testing number of days greater than zero

        int day = infection.getNumberOfDays();
        assertEquals(-1,day);
    }

    //R2 - virus infects instructors at a rate of 5 per day

    @Test
    public void testVirusInfectingRate() {

        int infectionRateOnInstructors = 5;

        int expectedInfectionRate = infection.getInfectionRate();

        assertEquals(infectionRateOnInstructors,expectedInfectionRate);
    }

    //R3 - virus infects instructors ata rate of 8 per day after 7 days
    @Test
    public void testVirusInfectingRateAfter7Days() {

        int infectionRateOnInstructors = 7;

        int expectedInfectionRate = infection.getInfectionRate();

        assertEquals(infectionRateOnInstructors,expectedInfectionRate);
    }


}