package com.example.screamitus_android;

public class Infection {

    private int noOfDays = 20;
    private int noOfInfectors = 0;
    private int infectorsOfDaysArray[]=new int[50];
    private int rate = 0;


    public int getNumberOfDays() {
        if (noOfDays <= 0){
            return -1;
        }
        return noOfDays;
    }


    public int[] getTheNoOfInfectors(){
        for (int i = 0; i <= noOfDays; i ++){
            if (i <= 7){
                noOfInfectors = noOfInfectors + 5;
                infectorsOfDaysArray[i] = noOfInfectors;
            }else {
                noOfInfectors = noOfInfectors + 7;
                infectorsOfDaysArray[i] = noOfInfectors;
            }
        }
        return  infectorsOfDaysArray;

    }

    //R3 & R4 - Get the infection rate before 7 days and after 7 days
    public int getInfectionRate() {
        int[] infectedArray = getTheNoOfInfectors();

        if (noOfDays <= 7) {
            rate = infectedArray[3] - infectedArray[2];
        }else {
            rate = infectedArray[11] - infectedArray[10];
        }
        return rate;

    }


    public int calculateTotalInfected(int day){
        getTheNoOfInfectors();
        return infectorsOfDaysArray[day];
    }
}
